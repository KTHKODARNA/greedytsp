import java.util.*;
import java.lang.*;

public class Main {

	Node[] input;
	Kattio io = new Kattio(System.in, System.out);
	int n;
	int k = 15;
	Double[][] distance;
	Node[][] neighborList;

	public static void main(String[] args) {
		Main m = new Main();
		m.run();
	}

	public void run() {
		readInput();
		if(n==1) {
			io.println("0");
			io.close();
		} else {
			createNeighborList();
			kattisGreedy();
		}

		
	}

	public void readInput() {
		n = io.getInt();
		input = new Node[n];

		int index = 0;
		while(io.hasMoreTokens()) {
			double x = io.getDouble();
			double y = io.getDouble();
			input[index] = new Node(x, y);
			index++;
		}

		if(n < k) {
			k = n;
		}
	}

	public Node[] findKNearest(int startNode) {
		Node[] tour = new Node[k];
		boolean[] used = new boolean[n];
		tour[0] = input[startNode];
		used[startNode] = true;
		for(int i=1; i<k; i++) {
			int best = -1;
			for(int j=0; j<n; j++) {
				if (!used[j] && (best == -1 || getDist(tour[i-1],input[j]) < getDist(tour[i-1], input[best]))) {
					best = j;
				}
			}
			tour[i] = input[best];
			tour[i].setIndexInInput(best);
			used[best] = true;
		}
		return tour;
	}

	public void createNeighborList() {
		neighborList = new Node[n][k];
		for(int i=0; i<n; i++) {
			neighborList[i] = findKNearest(i);
		}
	}

	public void kattisGreedy() {
		Node[] tour = new Node[n];
		boolean[] used = new boolean[n];
		tour[0] = input[0];
		tour[0].setIndexInInput(0);
		used[0] = true;
		int best;

		for(int i=1; i<n; i++) {
			best = -1;
			for(int j=1; j<k; j++) {
				if(!used[neighborList[tour[i-1].getIndexInInput()][j].getIndexInInput()] && (best == -1 || getDist(tour[i-1], neighborList[tour[i-1].getIndexInInput()][j].getIndexInInput() < getDist(tour[i-1], ))	)
					




				/*if(!used[neighborList[tour[i-1].getIndexInInput()][j].getIndexInInput()] && (best == -1 || getDist(tour[i-1], neighborList[tour[i-1].getIndexInInput()][j]) < getDist(tour[i-1], neighborList[tour[i-1].getIndexInInput()][best]))) {
					best = j;
				}*/
			}
			tour[i] = neighborList[i-1][best];
			used[best] = true;
		}

		printTour(tour);

	}



	public void printTour(Node[] tourToBePrinted) {
		for(Node n : tourToBePrinted) {
			System.err.println(n.getIndexInInput());
		}
		//System.err.close();
	}



	public double getDist(Node n1, Node n2) {
		double x = (n2.getX() - n1.getX()) * (n2.getX() - n1.getX());
		double y = (n2.getY() - n1.getY()) * (n2.getY() - n1.getY());

		return Math.sqrt(x+y);
	}



}