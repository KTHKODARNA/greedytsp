public class Node {
	
	private double x, y;
	private int index;

	public Node(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setIndexInInput(int i) {
		index = i;
	}

	public int getIndexInInput() {
		return index;
	}
}